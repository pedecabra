#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <libgen.h>
#include <assert.h>
#include <regex.h>

#include <magic.h>

/* Print Magic Error */
#define PME {

/* very stupid aproach, but very simple :) */
struct association_s {
    const char *filetype; /* filetype of the file */
    const char *viewapp;  /* path to the view program */
    const char *editapp;  /* path to the edit program */
};

#include "config.h"

int entry_count = sizeof(associations)/sizeof(struct association_s);

/* determine <file> filetype
 * 
 * if successfull <filetype> must be freed later by the user
 * returns true if successfull, false otherwise
 */

bool getfiletype(char *file, char **filetype)
{
    assert (file != NULL);

    magic_t mc; //magic_cookie
    char *magictype;
    int magictypelen;
    int stat;

    stat = access(file, F_OK);
    if (stat == -1) {
        perror("AH");
        return false;
    }

    mc = magic_open(MAGIC_NONE);
    if (mc == NULL) {
        perror("AH");
        return false;
    }

    stat = magic_load(mc, NULL); 
    if (stat == -1) {
        fprintf(stderr, "AH: %s", magic_error(mc));
        return false;
    }

    magictype = magic_file(mc, file);
    if (magictype == NULL) {
        fprintf(stderr, "AH: %s", magic_error(mc));
        return false;
    }
    //printf("Magic says: %s\n\n", magictype);

    /* "convert" (or copy) magic result to something local to close magic */
    magictypelen = strlen(magictype);
    (*filetype) = (char *)malloc(magictypelen+1);
    if (*filetype == NULL) {
        perror("AH");
        return false;
    }
    strncpy(*filetype, magictype, magictypelen+1);

    /* no more magic */
    magic_close(mc);

    return true;
}

/* match <filetype> against local database
 * 
 * <index> gets the index of the found entry, if successfull
 * returns true if entry was found, false otherwise
 */
bool match(char *filetype, int *index)
{
    assert(filetype != NULL);
    assert(index != NULL);

    bool found;
    int r;
    regex_t preg;

    /* this is a nasty piece of code. need to beautify!!! */
    for(r=0, found=false; (r < entry_count) && (found == false); ++r) {
        if (regcomp(&preg, associations[r].filetype, REG_EXTENDED | REG_NOSUB) != 0) {
            fprintf(stderr, "Unable to compile regex %s.\n", associations[r].filetype);
            return false;
        }
		/* true on substring match. to match whole string prepend with '^' and append '$'. */
        if (regexec(&preg, filetype, 0, 0, 0) == 0) {
            found = true;
        }
    }
    --r;
    regfree(&preg);

    *index = r;
    return found;
}

void main (int argc, char *argv[])
{
    bool stat;
    char *file;
    char *filetype;
    int index;
    char *newargv[3];

    if (argc != 3) {
        fprintf(stderr, "%s: Incorrect arguments.\n", argv[0]);
        fprintf(stdout, "Usage: %s v|e <file>\n", argv[0]);
        fprintf(stdout, "\t v: opens file with application registered to view\n");
        fprintf(stdout, "\t e: opens file with application registered to edit\n");
        exit(EXIT_FAILURE);
    }

    file = argv[2];

    stat = getfiletype(file, &filetype);
    if (stat == false) {
        fprintf(stderr, "Could not determine %s type.\n", file);
        exit(EXIT_FAILURE);
    }

    stat = match(filetype, &index); 
    if (stat == false) {
        fprintf(stderr, "Type '%s' not configured.\nAdd it to associations "\
                "array in config.h and recompile.\n", filetype);
        exit(EXIT_FAILURE);
    }

    free(filetype);

    switch (*argv[1]) {
        case 'e':
            newargv[0] = associations[index].editapp;
            newargv[1] = file;
            newargv[2] = (char *)NULL; 
            stat = execv(associations[index].editapp, newargv);
            perror("AH");
            break;
        case 'v':
            newargv[0] = associations[index].viewapp;
            newargv[1] = file;
            newargv[2] = (char *)NULL; 
            stat = execv(associations[index].viewapp, newargv);
            perror("AH");
            break;
        default:
            fprintf(stderr, "No application associated with the action %s for\
                    file %s.\n", argv[1], file);
            break;
    }

    exit(EXIT_FAILURE);
}
