include config.mk

# DO NOT CHANGE THE NEXT LINE. MAKE ALIASES ;)
CODENAME = pedecabra

SRC = pedecabra.c
OBJ = ${SRC:.c=.o}
DISTFILES = README Makefile TODO config.def.h config.mk ${SRC}

all: ${CODENAME}

${OBJ}: config.h config.mk

config.h: config.def.h
	@echo creating $@ from config.def.h
	@cp config.def.h $@

${CODENAME}: ${OBJ}
	@echo CC -o $@
	@${CC} -o $@ ${OBJ} ${LDFLAGS}

install: all
	@echo installing executable file to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f ${CODENAME} ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/${CODENAME}

uninstall:
	@echo removing executable file from ${DESTDIR}${PREFIX}/bin
	@${RM} ${DESTDIR}${PREFIX}/bin/${CODENAME}

clean:
	@echo cleaning
	@${RM} ${CODENAME} ${OBJ} ${CODENAME}-${VERSION}.tar.gz

dist: clean
	@echo creating dist tarball
	@mkdir -p ${CODENAME}-${VERSION}
	@cp ${DISTFILES} ${CODENAME}-${VERSION}
	@tar -cf ${CODENAME}-${VERSION}.tar ${CODENAME}-${VERSION}
	@gzip ${CODENAME}-${VERSION}.tar
	@${RM} -r ${CODENAME}-${VERSION}

