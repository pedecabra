/* {filetype, path_to_viewapp, path_to_editapp} */
struct association_s associations [] = {
    {"PDF document", "/usr/bin/xpdf", "/usr/bin/xpdf"},
    {"program text", "/bin/less", "/usr/bin/vim"},
    {"OpenDocument", "/usr/bin/soffice" /* -view */, "/usr/bin/soffice"},
    {"DVI file", "/usr/bin/xdvi", "/usr/bin/xdvi"},
    {"document text", "/bin/less", "/usr/bin/vim"},
};

